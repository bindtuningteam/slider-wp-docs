![advanced](../images/classic/12.advanced.png)

### Move to Slider Zone

<p class="alert alert-success">Requires the BindTuning Theme to be installed.</p>

Most BindTuning themes include a special zone for the slider, typically outside the page layout area. If you want to add the slider to the slider zone just enable the **Move to SliderZone** option.

By default, this option is **disable**.

___
### Language
 
From here you can select which language you want to be displayed by the web part. This will also translate the forms, but a refresh may be required before you see the changes.

By default, the BindTuning Web Part provides the language for:

- English
- Portuguese

If you'd like to localize the web part to your own language, please refer to [this article](https://support.bindtuning.com/hc/en-us/articles/115004585263).

___
### Retrieve rich content from source list 

This will add to the Web Part HTML content that you create on your source list. This option is only available with Smart Slider. 

<p class="alert alert-info">Support for this feature is not provided. Disables markdown. Recomended for experienced users only.</p>