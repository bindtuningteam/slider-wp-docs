**Option 1**

1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. Mouse hover the slider you want to edit, and click on the ✏️ (pencil) icon;

	![edit-slide.gif](../../images/classic/02.edit.slider1.gif)

3. You can check what you can edi in each section on the [Slider Settings](../../global/slidersettings);
4. Done editing? You can click on the **Preview** button to see how everything looks on the page, click on **Publish**, to save the changes.

	<p class="alert alert-success"><strong>Publish</strong>, <strong>Save as Draft</strong> and <strong>Submit for Review</strong> will only be visible if you previsouly checked the <strong>Enable Publishing Workflow</strong> option while setting the web part properties. You can read more about it <a href="../../workflow" target="_blank">here</a>.</p> 

	![save_buttons.png](../../images/classic/16.save.png)

___________
**Option 2**

1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. On the web part sidebar and click the **Manage Slider** icon;
3. The list of Slider will appear. Click the **Edit** icon to edit the Slider;
4. You can check what you can edit in each section on the [Slider Settings](../../global/slidersettings);
5. Done editing? You can click on the **Preview** button to see how everything looks on the page, click on **Publish**, to save the changes.

	![edit-slide.gif](../../images/classic/02.edit.slider2.gif)