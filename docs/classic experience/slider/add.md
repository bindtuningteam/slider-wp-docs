1. Open the page where you've added the web part; 
2. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
3. Click on the ➕ (plus) icon to add a new slider;

	![add_slider.gif](../../images/classic/04.add-slide.gif)

4. Fill out the form that pops up... You can check what you need to do in each section on the [Slider Settings](../../global/slidersettings);

5. After setting everything up, click on **Publish**. You can also preview the slider on the page before saving it, by clicking on the **Preview** button.

	![save_slider.png](../../images/classic/16.save.png)
	
	<p class="alert alert-success"><b>Publish</b>, <b>Save as Draft</b> and <b>Submit for Review</b> will only be visible if you previsouly checked the <b>Enable Publishing Workflow</b> option while setting the web part properties. You can read more about it <a href="../../workflow" target="_blank">here</a>.</p> 
	