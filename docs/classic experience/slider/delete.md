**Option 1**

1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. Mouse hover the Slider you want to delete, and click the **trash** icon to delete the Slider;
3. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the Slider will be removed.

![delete-slider](../../images/classic/03.delete.slider1.gif)

___________
**Option 2**


1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. On the web part sidebar and click the **Manage Slider** icon;
3. The list of Slider will appear. Click the **trash** icon to delete the Slider;
4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the Slider will be removed.	

![delete-slider](../../images/classic/03.delete.slider2.gif)