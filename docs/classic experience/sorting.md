![sortingitems](../images/classic/08.sortingitems.png)

<p class="alert alert-info">By default, this option contains the <b>Order By</b> on the column Sequence of the Slider list, which represent an option to insert a slider on a page with an certain order. For more information about this option check the <a href="../../global/slidersettings">Slider Settings</a>.</p>

<p class="alert alert-warning">If you're using the <b>Default Slider</b> this property will applied to <b>Slider lists</b>. If you're using the <b>Smart Slider</b>, it'll apply the order by to the <b>Source List</b>.</p>

Here you can define what will be the order of your slider using the internal name of one of your list colunms name. 

You will need to type in the internal name of the list column on the **Order by** text box - we will sort the slider according to the value of the cell name you enter.

Here is what you need to do: 

1. Access **Site contents** and open your list;
2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);
3. On the **Columns** section, click to open the colum name you want to use;
4. Inside, on the URL look for **"...Field=..."**.  
5. Copy the internal name;

	![internalcolumnname](../images/classic/14.internalcolumnname.png) 
	
6. Now paste the name on the text box;
7. Choose the order, **Ascending** or **Descending**. 

With **Add Order By**, you can add more sorting options. 