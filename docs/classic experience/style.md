![styles](../images/classic/13.styles.png)

### Slider Template

Select the layout for your slider. You can choose between:

- Carousel
- Small Thumbnails
- Large Thumbnails
- 3 Column News
- 4 Column News
- 3 Column Portfolio
- 4 Column Portfolio

___
### Slider Indicators 

Choose what Slider Indicators you want to use. You can shoo The options are:

- None
- Bullets
- Numbers

<p class="alert alert-info">If you pick the <b>Thumbnails</b> options this will not be available.</p>

___ 
### Slider Arrows 

Choose whether you want the arrow controls to be visbile on the page or if you want to hide them.

___ 
### Slider Interval

This setting defines the time delay, in seconds, between automatic slide rotations. If you don't want the slider to auto-rotate, set the interval to 0, and the rotation will only happen on click.

<p class="alert alert-success">By default, the auto-rotate slide interval is 5 seconds.</p>

___ 
### Image Background 

Choose between **Stretch** or **Cover**. 

With **Stretch**, background images will be stretched or squeezed to fit the max width and height, set on **Slider Height**.
With **Cover**, background images will be scaled to fit the available space without stretching the image.

___ 
### Image Alignment 

Defines the horizontal or vertical positioning for the Slider images to be focused. Note that you need to choose one value for **Horizontal** and other for **Vertical**.

|**Horizontal**|**Vertical**|
|--------------|------------|
|Rigth         |Top         |
|Center        |Center      |
|Left          |Bottom      |

___ 
### Item limit

Limit the number of Slider(s) that you show on the Slider Web Part. 

<p class="alert alert-info">If you have a CAML Query on the list with Row Limit, it'll get the items on that Limit and based on this limit show the items. The default value 0 will get all the items of the List View.</p>