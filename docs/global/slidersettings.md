![slider order](../images/classic/20.slider.order.png)

### Slide Order

First up is selecting your new Slide order. This order is based on a numerical order.  

- 0 - this slide will be the first one to appear
- 10 - this slide will be the second one to appear
- 20 - this slide will be the third one to appear
- ...

<p class="alert alert-success">Slides with the same value are ordered based on date of creation (older slider first). We recommend to use increments of 10 so then you can add Sliders on middle easily.</p>