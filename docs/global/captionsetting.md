![Slider Settings](../images/classic/21.slider.settings.png)

### Slide Background

Set a **color**/**image**/**video** for the slide background. For more information on how to use:
    
- For the color picker, refer to <a href="../format#color" target="_blank">this section</a>.
- For the image picker, refer to <a href="../image" target="_blank">this section</a>.
- For the video, you can use <a href="https://youtube.com" target="_blank">YouTube</a>, <a href="https://vimeo.com" target="_blank">Vimeo</a> and <a href="https://support.office.com/en-us/article/meet-office-365-video-ca1cc1a9-a615-46e1-b6a3-40dbd99939a6" target="_blank">Office 365 Video</a>.

To know more information about the **Renditions** used on the last dropdown, please visit the <a href="../renditions" target="_blank">next link</a>.

### Image Overlay

Add an Overlay color on top of the image. You can also add transparency.
 
### Thumbnail Image

Add the URL if you want to have a thumbnail image different from the Slider Image that you have on the slider.  

<p class="alert alert-info">This property will only be visible when the Thumbnail is active. If this value is empty will use the slide background as thumbnail.</p>

### Caption Text

Type the text you want for your slide. You can use the format buttons to define headings, alignment, weight, colors and add icons to the text.

To add text to your slide you will be using Markdown. If you don't know much about Markdown read more about it on our <a href="https://support.bindtuning.com/hc/en-us/articles/213784643" target="_blank">Markdown syntax in BindTuning</a> article.


### Caption Background Color

Sets a color for your slide caption. You can also add transparency.

### Caption Width

Sets a width for your slide caption. You can use px (pixel) or %(percentage) values.

### Caption Position

Here you can set the position of the caption inside the slide, both horizontally and vertically. Choose between **TOP**, **CENTER** and **BOTTOM** to set its vertical position or choose between **LEFT**, **CENTER** AND **RIGHT** to set its horizontal position. 

### Caption Margins 

Set a margin for the **Caption Position** for your slide. You can use px (pixel) or %(percentage) values. 