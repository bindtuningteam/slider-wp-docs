![publishing](../images/classic/19.publishing.png)

### Publishing Settings

Set the date range during which the slider will be live on the page by defining both the **Publish Start Date** and **Publish End Date.**