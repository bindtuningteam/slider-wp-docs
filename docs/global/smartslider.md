Let’s say we want to map the BindTuning Slider with a custom SharePoint List, with the Smart Slider will have this flexibility. The BindTuning Slider list will be used to create a map of the columns content that will be shown on the page based on the Custom List Items.

___
### Connect to Template List

1. Add the **BindTuning Slider** list to the zone **Template Slider**.
2. On the first text box insert one of the two:

	- **The URL of the list you created** 
	- **A URL of a site collection**	

3. Choose your **Filtering Options** to filter your list elements. You can choose to have:

    - **No Filters** - You show all the items on the list.
    - **CAML Query** - You can query the list to just show items based on the query built. 
    - **Select a View of the list** - You can pick the List View created and just show those elements on the list.

___
### Connect to Source List

The **Source list** is where your information will be pulled from. On the **Slider Caption**, we'll create the mapping used to present the information.

1. Click on **+ Add List** first;
2. On the first text box insert one of the two:

	- **The URL of the list you created** 
	- **A URL of a site collection**	

3. Choose your **Filtering Options** to filter your list elements. You can choose to have:

    - **No Filters** - You show all the items on the list.
    - **CAML Query** - You can query the list to just show items based on the query built. 
    - **Select a View of the list** - You can pick the List View created and just show those elements on the list.

![smartslider_config](../images/modern/15.smartslider_config.png)

<p class="alert alert-success">Need to add more lists? You can do it by clicking on <strong>Add List</strong>.</p>


You can combine the other Web Part Properties to have the desired look:

- <a href="../classic/general" target="_blank">Classic Experience</a>
- <a href="../modern/general" target="_blank">Modern Experience</a>

___
### Captions Settings

1. Depending on your SharePoint experience, **Add a slider** to the Web Part:

- <a href="../classic/slider/add" target="_blank">Classic Experience</a>
- <a href="../modern/slider/add" target="_blank">Modern Experience</a>


<strong>For the background</strong>, you can use a field from the source list to add an **image/color/video**. Simply choose the image option in the background field and place the column's internal name in brackets: <strong>{{FIELD_NAME}}</strong>, as shown in the image below.


<strong>For the content</strong>, you can use a field from the queried list to add text. Simply type the column's internal name in brackets: <b>{{FIELD_NAME}}</b>, as shown in the image below. You can combine static text with the Column's Internal. 

<p class="alert alert-info">You can still use the <a href="./format" target="_blank">MarkDown</a> syntax as normal.</p>

![smartslider_content](../images/modern/14.smartslider_content.png)

Here is what you need to do to check your column names: 

1. Access **Site contents** and open your list;
2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);
3. On the **Columns** section, click to open the colum name you want to use;
4. Inside, on the URL look for **"...Field=..."**.  
5. Copy the internal name;
6. Now paste the name on the text box;



 
![internalcolumnname](../images/classic/14.internalcolumnname.png) 

After setting everything up, click on **Publish**. You can also preview the Slider on the page before saving it, by clicking on the **Preview** button.


![webpartproperties_18.png](../images/modern/04.example.gif)
___ 
### How to use CAML Query

You can use a free 3rd party software for creating the query, like this one👉<a href="http://www.u2u.be/software/" target="_blank">http://www.u2u.be/software/</a>. Then you can copy the query and use it as a filter. Below is an example with multiple filters allowed of a query you can have.

   <p class="alert alert-success"> If you’re using SharePoint 2013, install the U2U CAML Query Builder for SharePoint 2013 (Standalone Edition).</p>

    <View>
    <Query>
        <Where>
            <And>// have to meet this 2 conditions
                <Eq> //Equal
                    <FieldRef Name='PromotedState' />
                    <Value Type='Number'>2</Value>
                </Eq>
                <Leq> //Lower or Equal
                    <FieldRef Name="Created" />
                    <Value Type="DateTime" Offset="-90"> // -90 days from today
                        <Today />
                    </Value>
                </Leq>
            </And>
        </Where>
        <OrderBy>
            <FieldRef Name='Created' Ascending='False'></FieldRef> // Most Recent to older created element
        </OrderBy>
    </Query>
    <RowLimit>3</RowLimit> // Number of items that you want to show
    </View>