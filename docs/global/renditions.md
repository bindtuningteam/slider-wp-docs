The SharePoint has functionality that after you upload an image for an Asset Library will create pictures with multiple resolutions that you can use on pages which will be more lighter for the browser to load and consequently improve the performance of the site.

For more information about this functionality, please visit the next links:

- [Renditions](https://docs.microsoft.com/en-us/sharepoint/dev/general-development/sharepoint-design-manager-image-renditions)
- [Create Rendition](https://docs.microsoft.com/en-us/sharepoint/dev/general-development/sharepoint-design-manager-image-renditions#create-an-image-rendition)
- [Crop Rendition](https://docs.microsoft.com/en-us/sharepoint/dev/general-development/sharepoint-design-manager-image-renditions#crop-an-image-rendition) 