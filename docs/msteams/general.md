![06.options](../images/modern/07.options.png)

On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Create New BT Slider List](./createlist.md)
- [List Settings](./slider.md)
- [Slider Styles](./style.md)
- [Web Part Appearance](./appearance.md)
- [Advanced Options](./advanced.md)
- [Performance](./performance.md)
- [Web Part Messages](./message.md)