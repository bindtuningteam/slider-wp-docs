1. Open the Team on **Teams panel** that you intend to remove an item of the Web Part;
2. Click on the **Settings** button.

	![settings_delete.png](../../images/msteams/setting_edit.png)

3. Click on the **Manage Slides** icon;

	![Manage](../../images/msteams/manage_slide.png)

4. The list of slides will appear. Click the **trash can** icon to delete the slide that you want to remove;

	![Delete](../../images/classic/03.delete-slide.png)

5. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the alert will be removed.	
