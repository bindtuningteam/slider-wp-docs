1. Open the Team on **Teams panel** that you intend to add an new item to the Web Part; 
2. Click on the **Settings** button. 

	![setting_edit.png](../../images/msteams/setting_edit.png)

2. Click on the **[+]** button to add a new Slide;
3. Fill out the form that pops up. You can check out what you need to do in each setting in the <a href="../../global/captionsetting" target="_blank">Slider Settings</a> section;
4. After setting everything up, click on the **Preview** button if you want to see how everything looks on the page, or click on **Save**.

	![Add_new_alert_teams.gif](../../images/msteams/add_new_slide_teams.gif)
