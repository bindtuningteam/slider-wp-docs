1. Open the Team on **Teams panel** that you intend to edit an item of the Web Part; 
2. Click on the **Settings** button.

	![settings_delete.png](../../images/msteams/setting_edit.png)

3. Click on the **Manage Slide** icon;

	![Edit](../../images/classic/02.edit.slider.png)

4. Change the <a href="../../global/slidersettings">Slide Settings</a> as necessary to re-configure the slide.

4. After setting everything up, click on the **Preview** button if you want to see how everything looks on the page, or click on **Save**.