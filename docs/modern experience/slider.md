### Default Slider

![slider_options](../images/modern/13.slider_options.png)

Here you will need to connect your web part with a SharePoint list. Without it, the web part will not function and an notification message will appear. Let's get to it:

1. Click **Add Slider**;
2. On the first text box insert one of the two:

	- **The URL of the list you created** 
	- **A URL of a site collection**	 
  <p class="alert alert-info">Use relative paths for this field. So instead of using an URL like <strong>https://company.sharepoint.com/sites/Home</strong>, you should use something like <strong>/sites/Home/</strong>.</p>

3. Second box, select the list you have created to be used if not already selected;
4. Choose your **Filtering Options** to filter your list elements. You can choose to have:

- **No Filters** - You show all the items on the list.
- **CAML Query** - You can query the list to just show items based on the query built.
- **Select a View of the list** - You can pick the List View created and just show those elements on the list.

5. When you're done, click on the 💾(save) icon to save. 

Now you're ready to insert images on your **Slider Web Part**. Visit the next link for [create yours Slider](./slider/add.md).

___
### Item Sorting Options

<p class="alert alert-info">By default, this option contains the <b>Order By</b> on the column Sequence of the Slider list, which represent an option to insert a slider on a page with an certain order. For more information about this option check the <a href="../../global/slidersettings">Slider Settings</a>.</p>

<p class="alert alert-warning">If you're using the <b>Default Slider</b> this property will applied to <b>Slider lists</b>. If you're using the <b>Smart Slider</b>, it'll apply the order by to the <b>Source List</b>.</p>

Here you can define what will be the order of your slider using the internal name of one of your list colunms name. 

You will need to type in the internal name of the list column on the **Order by** text box - we will sort the slider according to the value of the cell name you enter.

Here is what you need to do: 

1. Access **Site contents** and open your list;
2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);
3. On the **Columns** section, click to open the colum name you want to use;
4. Inside, on the URL look for **"...Field=..."**.  
5. Copy the internal name;

	![internalcolumnname](../images/classic/14.internalcolumnname.png) 
	
6. Now paste the name on the text box;
7. Choose the order, **Ascending** or **Descending**. 

With **Add Order By**, you can add more sorting options. 

___
### How to use CAML Query

You can use a free 3rd party software for creating the query, like this one👉<a href="http://www.u2u.be/software/" target="_blank">http://www.u2u.be/software/</a>. Then you can copy the query and use it as a filter. Below is an example with multiple filters allowed of a query you can have.

   <p class="alert alert-success"> If you’re using SharePoint 2013, install the U2U CAML Query Builder for SharePoint 2013 (Standalone Edition).</p>

    <View>
    <Query>
        <Where>
            <And>// have to meet this 2 conditions
                <Eq> //Equal
                    <FieldRef Name='PromotedState' />
                    <Value Type='Number'>2</Value>
                </Eq>
                <Leq> //Lower or Equal
                    <FieldRef Name="Created" />
                    <Value Type="DateTime" Offset="-90"> // -90 days from today
                        <Today />
                    </Value>
                </Leq>
            </And>
        </Where>
        <OrderBy>
            <FieldRef Name='Created' Ascending='False'></FieldRef> // Most Recent to older created element
        </OrderBy>
    </Query>
    <RowLimit>3</RowLimit> // Number of items that you want to show
    </View>


___
### Smart Slider

To know more about this functionality that allows you to connect the Slider List with any other list where your images or content will be created, please visit the section of [Global Slider Options](./../global/smartslider.md)  