**Option 1**

1. Click on the **Edit** button;

    ![edit-page](../../images/modern/01.edit.modern.png)

2. Mouse over the Slider you intend to delete and click the **pencil** icon to edit the Slider:
3. Fill out the form that pops up. You can check out what you need to do in each setting in the <a href="../../global/captionsetting" target="_blank">Slider Settings</a> section ;

4. After setting everything up, click on the **Preview** button if you want to see how everything looks on the page, or click on **Save**.

![delete Slider](../../images/modern/03.edit.slider1.gif)
__________
**Option 2**

1. Click on the **Edit** button;

    ![edit-page](../../images/modern/01.edit.modern.png)

2. On the web part sidebar and click the **Manage Slider** icon;

3. The list of Slider will appear. Click the **pencil** icon to edit the slider;

3. Fill out the form that pops up. You can check out what you need to do in each setting in the <a href="../../global/captionsetting" target="_blank">Slider Settings</a> section ;

4. After setting everything up, click on the **Preview** button if you want to see how everything looks on the page, or click on **Save**.

![delete Slider](../../images/modern/03.edit.slider2.gif)

