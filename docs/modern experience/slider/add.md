1. Click on the **Edit** button;

	![edit-page](../../images/modern/01.edit.modern.png)

2. Mouse hover the Web Part and click on the **[+]** button to add a new **Slider**;

	![slider](../../images/modern/01.add.slider.gif)

3. Fill out the form that pops up. You can check out what you need to do in each setting in the <a href="../../global/captionsetting" target="_blank">Slider Settings</a> section ;

4. After setting everything up, click on the **Preview** button if you want to see how everything looks on the page, or click on **Save**.