**Option 1**

1. Click on the **Edit** button;

    ![edit-page](../../images/modern/01.edit.modern.png)

2. Mouse over the Slider you intend to delete and click the **trash** icon to delete the Slider
3. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the Slider will be removed.	

![delete Slider](../../images/modern/02.delete.slider1.gif)
__________
**Option 2**

1. Click on the **Edit** button;

    ![edit-page](../../images/modern/01.edit.modern.png)

2. On the web part sidebar and click the **Manage Slider** icon;

3. The list of Slider will appear. Click the **trash** icon to delete the Slider that you want to remove;

![delete Slider](../../images/modern/02.delete.slider2.gif)

